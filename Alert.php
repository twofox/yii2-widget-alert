<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014
 * @package yii2-widgets
 * @subpackage yii2-widget-alert
 * @version 1.1.0
 */

namespace twofox\alert;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * Extends the \yii\bootstrap\Alert widget with additional styling and auto fade out options.
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 1.0
 */
class Alert extends \kartik\alert\Alert
{
    /**
     * @var string the type of the alert to be displayed. One of the `TYPE_` constants.
     * Defaults to `TYPE_INFO`
     */
    public $type = self::TYPE_INFO;

    /**
     * @var string the icon type. Can be either 'class' or 'image'. Defaults to 'class'.
     */
    public $iconType = 'class';

    /**
     * @var string the class name for the icon to be displayed. If set to empty or null, will not be
     * displayed.
     */
    public $icon = '';

    /**
     * @var array the HTML attributes for the icon.
     */
    public $iconOptions = [];

    /**
     * @var string the title for the alert. If set to empty or null, will not be
     * displayed.
     */
    public $title = '';

    /**
     * @var array the HTML attributes for the title. The following options are additionally recognized:
     * - tag: the tag to display the title. Defaults to 'span'.
     */
    public $titleOptions = ['class' => 'kv-alert-title'];

    /**
     * @var bool show title separator. Only applicable if `title` is set.
     */
    public $showSeparator = false;

    /**
     * @var integer the delay in microseconds after which the alert will be displayed.
     * Will be useful when multiple alerts are to be shown.
     */
    public $delay;
    public $speed;

    /**
     * Register client assets
     */
    protected function registerAssets()
    {
        $view = $this->getView();
        \kartik\alert\AlertAsset::register($view);

        if ($this->delay > 0) {
            $js = 'setTimeout(function(){
                        jQuery("#' . $this->options['id'] . '").slideUp(' . $this->speed . ', function() {
                            $(this).remove();
                        });
                   }, ' . $this->delay . ');
            ';
            $view->registerJs($js);
        }
    }
}
